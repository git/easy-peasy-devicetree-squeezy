#!/bin/sh
set -e

usage () {
	echo "usage: easy-peasy-devicetree-squeezy boardname kerneltarball dtsfile destination | --debian boardname" >&2
	exit 1
}

if [ "$1" = "--debian" ]; then
	boardname="$2"
	if [ -z "$boardname" ]; then
		usage
	fi
	apt-get -y install linux-source
	kvers="$(dpkg -s linux-source | grep '^Version:' | cut -d ' ' -f 2 | cut -d. -f 1,2)"
	echo "Compiling /etc/easy-peasy-devicetree-squeezy/my.dts for kernel version $kvers..."
	if ! easy-peasy-devicetree-squeezy --debian-internal "$boardname" "$kvers"; then
		echo "Compilation of /etc/easy-peasy-devicetree-squeezy/my.dts failed, aborting installation!" >&2
		exit 1
	fi
	flash-kernel
	# needs to come before the flash-kernel script, which it luckily does
	initramfs_hook="/etc/initramfs/post-update.d/easy-peasy-devicetree-squeezy"
	( 
		echo "#!/bin/sh"
		echo "exec easy-peasy-devicetree-squeezy --debian-internal \"$boardname\"" '"$1"'
	) > "$initramfs_hook"
	chmod +x "$initramfs_hook"
	kernel_hook="/etc/kernel/postinst.d/za-easy-peasy-devicetree-squeezy"
	cp -a "$initramfs_hook" "$kernel_hook"
	echo "Device tree will be automatically updated when the kernel or initramfs is upgraded."
	exit 0
elif [ "$1" = "--debian-internal" ]; then
	boardname="$2"
	kvers="$(echo "$3" | cut -d. -f 1,2)"
	kerneltarball="/usr/src/linux-source-$kvers.tar.xz"
	dtsfile="/etc/easy-peasy-devicetree-squeezy/my.dts"
	destination="/etc/flash-kernel/dtbs/$boardname.dtb"
	if [ ! -e "$kerneltarball" ]; then
		echo "easy-peasy-devicetree-squeezy: $kerneltarball not present. Perhaps you need to: apt-get install linux-source" >&2
		exit 1
	fi
else
	boardname="$1"
	kerneltarball="$2"
	dtsfile="$3"
	destination="$4"
fi

if [ -z "$boardname" ] || [ -z "$kerneltarball" ] || [ -z "$dtsfile" ] || [ -z "$destination" ]; then
	usage
fi
if [ ! -e "$dtsfile" ]; then
	echo "$dtsfile does not exist" >&2
	exit 1
fi

cachedir="$HOME/.cache/easy-peasy-devicetree-squeezy"

ident=$(stat -c "%d %i" "$kerneltarball")
if [ ! -e "$cachedir/.ident" ] || [ "$(cat "$cachedir/.ident")" != "$ident" ]; then
	echo "Extracting linux device tree sources to $cachedir"
	rm -rf "$cachedir"
	mkdir -p "$cachedir"
	pv "$kerneltarball" | \
		(cd "$cachedir" && \
			tar Jx --wildcards '*/include/dt-bindings' '*.dts' '*.dtsi' \
				--strip-components=1
		)
	printf "$ident" > "$cachedir/.ident"
fi

boarddts="$(find "$cachedir" | grep "/$boardname.dts" | head -n 1)"
if [ -z "$boarddts" ]; then
	echo "Did not find any $boardname.dts file in the linux kernel tree sources in $cachedir" >&2
	exit 1
fi

preproc () {
	cpp -nostdinc \
		-I "$(dirname "$boarddts")" \
		-I "$cachedir/include" \
		-undef -x assembler-with-cpp "$1"
}

compile () {
	dtc -O dtb -b 0 -o "$1" -
}

(preproc "$boarddts" && preproc "$dtsfile") | compile "$destination"
echo "Merged device tree compilation successful."
