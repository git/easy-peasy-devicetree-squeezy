## Easy peasy devicetree squeezy

So you want to add something to the devicetree for your board, but don't
have a bootloader or kernel that supports devicetree overlays. Or, device
tree overlays are too annoying/hard to use. This program makes it easy
peasy to squeeze together the devicetree for your board with whatever
additions you need.

## Installation

Install easy-peasy-devicetree-squeezy somewhere in PATH:  
`sudo make install`

You'll also need the `pv` utility,
and the [device tree compiler](https://git.kernel.org/pub/scm/utils/dtc/dtc.git/),
and `cpp` and the kernel source. Install that stuff on Debian with:  
`sudo apt-get install pv device-tree-compiler cpp linux-source`

## Usage

easy-peasy-devicetree-squeezy boardname kerneltarball dtsfile destination

The boardname is the name that the kernel uses for your board, 
eg sun7i-a20-cubietruck. For a list, take a look at
`ls /usr/lib/linux-image-$(uname -r)`

The kerneltarball is the source for your kernel.
It's expected to be in tar.xz format.

The dtsfile contains the additional nodes you want to add to the device
tree. See below for details about writing this file.

After extracting the sources from the kernel (cached to
`~/.cache/easy-peasy-devicetree-squeezy`), it will compile the device
tree, and write it to the specified destination.

## Example

easy-peasy-devicetree-squeezy sun7i-a20-cubietruck \
	/usr/src/linux-source-4.14.tar.xz my.dts out.dtb

## Writing the dtsfile

Device tree overlay files are a tweaked version of the device tree file
format, that modify the board's main device tree.
easy-peasy-devicetree-squeezy does *not* use such overlay files. Instead,
it simply concacenates the kernel's device tree for your board with the
dtsfile you provide.

So, in your dtsfile, just write the device tree nodes you need to add;
they will be merged together with the kernel's device tree nodes for your
board.

You can use labels, such as "&pio" that are defined in the board's device
tree. The dtsfile is preprocessed with cpp, and all the kernel includes
are available, so you can include things like gpio.h and then use 
symbols like `GPIO_ACTIVE_HIGH`.

Here's an example, that takes advantage of these nice features:

	/* Device tree addition enabling onewire sensors
	 * on CubieTruck GPIO pin PG8 */
	#include <dt-bindings/gpio/gpio.h>
	
	/ {
        	onewire_device {
	                compatible = "w1-gpio";
	                gpios = <&pio 6 8 GPIO_ACTIVE_HIGH>; /* PG8 */
	                pinctrl-names = "default";
	                pinctrl-0 = <&my_w1_pin>;
	        };
	};
	
	&pio {
		my_w1_pin: my_w1_pin@0 {
			allwinner,pins = "PG8";
			allwinner,function = "gpio_in";
		};
	};

This example is pretty much taken verbatim from an example
in <http://linux-sunxi.org/1-Wire>. The example on that page assumed
you were going to go edit your kernel source and insert stuff into its
device tree, and recompile the kernel. With easy-peasy-devicetree-squeezy,
you don't need to modify the kernel sources.

## Automation on Debian

On Debian, easy-peasy-devicetree-squeezy integrates with flash-kernel,
initramfs-tools, and the kernel packages, so that whenever the kernel
gets updated, the device tree is rebuilt, including your additions.

All you have to do is put the device tree nodes you want to add into
`/etc/easy-peasy-devicetree-squeezy/my.dts`, and then run
"sudo easy-peasy-devicetree-squeezy --debian boardname" to set it up,
replacing boardname with eg sun7i-a20-cubietruck.

## Deprecation

This program has just been written, but it is already deprecated.
(The silly name is kind of a hint!)

Device tree overlay support in Linux is a work in progress. Once it gets to
the point that the mainline kernel supports loading overlays on the fly,
or there's good support for loading overlays in u-boot, there will not
be a lot of reason to keep easy-peasy-devicetree-squeezy around, probably.

It should not be too hard to convert your dtsfile into an overlay file
then.

## License

easy-peasy-devicetree-squeezy is copyright 2018 by Joey Hess
<id@joeyh.name>, and is licensed under the terms of the GNU GPL version 3
or higher.
